#!/usr/local/bin/fish

# from https://gist.github.com/sorahn/0fd41899f11cdbb78df370d5672713fc

# Generate a [hopefully] readable color based on the hostname

# Doing some reading on HSL, I noticed a pattern.  Keeping S(aturation) and
# L(ightness) the same, I can simulate making only adjusting the Hue on an HSL slider by
# keeping the other 2 numbers in an RGB triplet 00, and FF.
#
# Examples:
#   00FFad
#   FFb200
#   1b00FF
#
# The 3 numbers that we have below are:
#   pos_x: the position of the random octet.
#   val_x: the value of the random octet.
#   pos_f: the position of the FF octet relative to the XX octet. (0 left, 1 right)

function hostname_as_color
  set hash (hostname | md5sum)
  set pos_x (math (printf "%d" "0x"(echo $hash | cut -c1-2))%3)
  set val_x (echo $hash | cut -c3-4)
  set pos_f (math (printf "%d" "0x"(echo $hash | cut -c5-6))%2)

  switch $pos_f
    case 0
      switch $pos_x
        case 0; set color ""(echo $val_x)"FF00"
        case 1; set color "00"(echo $val_x)"FF"
        case 2; set color "FF00"(echo $val_x)""
      end
    case 1
      switch $pos_x
        case 0; set color ""(echo $val_x)"00FF"
        case 1; set color "FF"(echo $val_x)"00"
        case 2; set color "00FF"(echo $val_x)""
      end
  end

  echo $color
end
