function fish_prompt --description 'Write out the prompt'
    # Save our status
    set -l last_status $status

    set -l last_status_string ""
    if [ $last_status -ne 0 ]
        printf "%s%d%s " (set_color red --bold) $last_status (set_color normal)
    end

    set -l color_cwd
    set -l suffix
    set -g fish_prompt_pwd_dir_length 0

    switch $USER
        case root toor
            if set -q fish_color_cwd_root
                set color_cwd $fish_color_cwd_root
            else
                set color_cwd $fish_color_cwd
            end
            set suffix '#'
        case '*'
            set color_cwd $fish_color_cwd
            set suffix '>'
    end    

#    echo -n -s "$USER" @ (prompt_hostname) ':' (set_color $color_cwd) (prompt_pwd) (set_color normal) "$suffix "

    set -l string (set_color magenta) "$USER" (set_color normal) '@' (set_color green) (prompt_hostname) (set_color normal) ':' (set_color blue) (prompt_pwd) (set_color normal) "$suffix "

#    set -l string "$USER" '@' (set_color green) (prompt_hostname) (set_color normal) ':' (set_color blue) (prompt_pwd) (set_color normal) "$suffix "

    # https://fishshell.com/docs/current/commands.html#string
    set -l string (string replace -- (hostname) (set_color (hostname_as_color))(hostname)(set_color normal) $string)
#    set -l string (string replace -- "$RELEASE" (set_color yellow)"$RELEASE"(set_color blue) $string)
    set -l string (string replace -- "$RELEASE" (set_color (projname_as_color))"$RELEASE"(set_color blue) $string)
    set -l string (string replace -- "$USER" (set_color (username_as_color))"$USER"(set_color blue) $string)

#       set -l red          (set_color red)
#       set -l normal       (set_color normal)
#       set -l colored_path (string replace -r "$RELEASE" ${red} "$RELEASE" ${normal} $string)

    echo -n -s $string
#    echo -n -s $colored_path
end
