;; from http://www.lunaryorn.com/posts/my-emacs-configuration-with-use-package.html

(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/"))

(package-initialize)

;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; https://github.com/jwiegley/use-package


(add-to-list 'default-frame-alist '(height . 60))
(add-to-list 'default-frame-alist '(width . 140))
(add-to-list 'default-frame-alist '(left-fringe . 11))
;(add-to-list 'default-frame-alist '(right-fringe . 0))

;;(require 'package) ;; You might already have this line
;;(add-to-list 'package-archives
;;             '("melpa" . "http://melpa.org/packages/"))
;;(when (< emacs-major-version 24)
;;  ;; For important compatibility libraries like cl-lib
;;  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
;;(package-initialize) ;; You might already have this line


(add-to-list 'load-path "~/emacs/emacsgoodies")

;;*=======================
;;* Get rid of yes-or-no questions
(fset 'yes-or-no-p 'y-or-n-p)

;;*=======================
(turn-off-auto-fill)
;;(remove-hook 'text-mode-hook #'turn-on-auto-fill)

;;*=======================
;; make scripts executable automatically
;(require 'shebang)
;; or use this:
(add-hook 'after-save-hook 'executable-make-buffer-file-executable-if-script-p)

;;*=======================
;; https://github.com/zk-phi/sublimity
(require 'sublimity)
;; (require 'sublimity-scroll)
(require 'sublimity-map) ;; experimental
(require 'sublimity-attractive)
(sublimity-mode 1)
(sublimity-attractive-hide-bars)
(sublimity-attractive-hide-vertical-border)
;(sublimity-attractive-hide-fringes)
;(sublimity-attractive-hide-modelines)
(sublimity-map-set-delay nil)

;;(add-hook 'sublimity-map-setup-hook
;;          (lambda ()
;;          ;; (setq buffer-face-mode-face '(:family "Monospace"))
;;            ;; (setq buffer-face-mode-face '(:family "LucidaTypewriter"))
;;             (setq buffer-face-mode-face '(:family "-adobe-helvetica-medium-r-normal--10-*-75-75-p-56-iso8859-15"))
;;            ;; 
;;            (buffer-face-mode)))

(setq text-scale-mode-step 1.1)  ; 1.2 is the default
(global-set-key [C-mouse-4] 'text-scale-decrease)
(global-set-key [C-mouse-5] 'text-scale-increase)

;;*=======================
;; https://github.com/zk-phi/indent-guide
(require 'indent-guide)
(indent-guide-global-mode)
;(copy-face 'font-lock-comment-face 'indent-guide-face)
;(copy-face 'font-lock-type-face 'indent-guide-face)
(set-face-foreground 'indent-guide-face "orange")
;;(setq indent-guide-char "|") ; default
;;(setq indent-guide-char "⁞") ; not without Xft
;;(setq indent-guide-char "⁝") ; not without Xft
;;(setq indent-guide-char "⁚") ; not without Xft
;;(setq indent-guide-char "⋮")
;;(setq indent-guide-char "⎸")
;;(setq indent-guide-char "⎹")
;;(setq indent-guide-char "⏐") ; not without Xft
;;(setq indent-guide-char "│")
;;(setq indent-guide-char "┆")
;;(setq indent-guide-char "┇")
;;(setq indent-guide-char "┊")
;;(setq indent-guide-char "┋")
(setq indent-guide-char "╎")
;;(setq indent-guide-char "╏")
;;(setq indent-guide-char "❘") ; not without Xft
;;(setq indent-guide-char "︱")
;;(setq indent-guide-char "︳")
;;(setq indent-guide-char "￨") ; not without Xft
;;(setq indent-guide-char "￤")
;;(setq indent-guide-char "⃦")


;;*=======================
;; https://github.com/zk-phi/git-complete
(require 'git-complete)
(global-set-key (kbd "C-c C-g") 'git-complete)

;;*=======================
;;(require 'mediawiki)

;;*=======================
;;;; EUDC (from https://www.gnu.org/software/emacs/manual/eudc.html#LDAP-Configuration)
(require 'eudc)

;;(require 'ldap)
;;(require 'eudc)
;;
;;(setq eudc-default-return-attributes nil
;;      eudc-strict-return-matches nil)
;;
;;(setq ldap-ldapsearch-args (quote ("-tt" "-LLL" "-x")))
;;(setq eudc-inline-query-format '((name)
;;                                 (firstname)
;;                                 (firstname name)
;;                                 (email)
;;                                 ))
;;
;;(setq ldap-host-parameters-alist
;;      (quote (("ldap://de.bosch.com:3268"
;;               base "dc=de,dc=bosch,dc=com"
;;               binddn "de\\maq3rt"
;;               passwd ldap-password-read
;;               auth simple))))
;;
;;(eudc-set-server "ldap://de.bosch.com:3268" 'ldap t)
;;(setq eudc-server-hotlist '(("ldap://de.bosch.com:3268" . ldap)))
;;(setq eudc-inline-expansion-servers 'hotlist)
;;
(with-eval-after-load "message"
  (define-key message-mode-map (kbd "TAB") 'eudc-expand-inline))
;;(customize-set-variable 'eudc-server-hotlist
;;                        '(("ldap://de.bosch.com:3268" . ldap)))
;;(customize-set-variable 'ldap-host-parameters-alist
;;                        '(("ldap://de.bosch.com:3268"
;;                           base "dc=de,dc=bosch,dc=com"
;;                           binddn "de\\maq3rt"
;;                           passwd ldap-password-read
;;                           auth simple)))
;;
;;(setq eudc-strict-return-matches nil)

;;*=======================
;; from http://emacs.stackexchange.com/questions/2884/the-old-how-to-fold-xml-question
(require 'hideshow)
(require 'sgml-mode)
(require 'nxml-mode)
(add-to-list 'hs-special-modes-alist
             '(nxml-mode
               "<!--\\|<[^/>]*[^/]>"
               "-->\\|</[^/>]*[^/]>"

               "<!--"
               sgml-skip-tag-forward
               nil))
(add-hook 'nxml-mode-hook 'hs-minor-mode)
;; optional key bindings, easier than hs defaults
(define-key nxml-mode-map (kbd "C-c h") 'hs-toggle-hiding)

(require 'hideshowvis)
(hideshowvis-symbols)

(autoload 'hideshowvis-minor-mode
  "hideshowvis"
  "Will indicate regions foldable with hideshow in the fringe."
  'interactive)

;;*=======================

(use-package highlight-symbol
  :ensure t
  :init 
  (add-hook 'prog-mode-hook 'highlight-symbol-mode)
  (add-hook 'prog-mode-hook 'highlight-symbol-nav-mode)
  (add-hook 'specman-mode-hook 'highlight-symbol-mode)
  (add-hook 'specman-mode-hook 'highlight-symbol-nav-mode)
  :config
  (setq highlight-symbol-idle-delay 1.0
        highlight-symbol-on-navigation-p t)
  :diminish highlight-symbol-mode)

;;;; from http://stackoverflow.com/questions/16048231/how-to-enable-a-non-global-minor-mode-by-default-on-emacs-startup
;;(define-globalized-minor-mode my-global-highlight-symbol-mode highlight-symbol-mode
;;  (lambda () (highlight-symbol-mode 1)))
;;;(my-global-highlight-symbol-mode 0)
;;;(my-global-highlight-symbol-mode)
;;(add-hook 'prog-mode-hook    'highlight-symbol-mode)
;;(add-hook 'specman-mode-hook 'highlight-symbol-mode)
;;(add-hook 'text-mode-hook    'highlight-symbol-mode)
;;(setq auto-mode-alist
;;       (append (list
;;                (cons "transcript"  'highlight-symbol-mode)
;;                (cons "trace\\.log" 'highlight-symbol-mode)
;;                (cons "\\.e\\'"     'highlight-symbol-mode)
;;                (cons "\\.va$"      'highlight-symbol-mode)
;;                (cons "\\.v$"       'highlight-symbol-mode)
;;                (cons "\\.vh$"      'highlight-symbol-mode)
;;                (cons "\\.vams$"    'highlight-symbol-mode)
;;                (cons "\\.dv$"      'highlight-symbol-mode)
;;                (cons "\\.va$"      'highlight-symbol-mode)
;;                (cons "\\.vsif$"    'highlight-symbol-mode)
;;                (cons "\\.vhd$"     'highlight-symbol-mode)
;;                (cons "\\.vhdl$"    'highlight-symbol-mode)
;;                (cons "\\.vhms$"    'highlight-symbol-mode)
;;                )
;;               auto-mode-alist))

;; Performance Improvement: from https://github.com/nschum/highlight-symbol.el/issues/26
;; (defun highlight-symbol-add-symbol-with-face (symbol face)
;;   (save-excursion
;;     (goto-char (point-min))
;;     (while (re-search-forward symbol nil t)
;;       (let ((ov (make-overlay (match-beginning 0)
;;                               (match-end 0))))
;;         (overlay-put ov 'highlight-symbol t)
;;         (overlay-put ov 'face face)))))
;;
;; (defun highlight-symbol-remove-symbol (_symbol)
;;   (dolist (ov (overlays-in (point-min) (point-max)))
;;     (when (overlay-get ov 'highlight-symbol)
;;       (delete-overlay ov))))

;;*=======================
(autoload 'specman-mode "specman-mode" "Specman code editing mode" t)
(setq auto-mode-alist
       (append (list
                (cons "\\.e\\'" 'specman-mode)
                (cons "\\.ecom\\'" 'specman-mode)
                (cons "\\.erld\\'" 'specman-mode))
               auto-mode-alist))
(require 'speedbar)
(require 'sr-speedbar)
(speedbar-add-supported-extension ".e")
;; from http://emacs.stackexchange.com/questions/29328/how-to-display-full-name-of-path-in-sr-speedbar-or-wrap-it-if-it-exceeds-the-win
(add-hook 'speedbar-mode-hook (lambda () (setq truncate-lines nil)))

;;*=======================
(add-to-list 'load-path "~/emacs/flycheck-hdl-irun")
(require 'flycheck-hdl-irun)
;; (add-hook 'vhdl-mode-hook 'flycheck-mode)
;; (add-hook 'verilog-mode-hook 'flycheck-mode)
;; (add-hook 'systemc-mode-hook 'flycheck-mode)
(add-hook 'specman-mode-hook 'flycheck-mode)

;;*=======================
;; https://inside-docupedia.bosch.com/confluence/display/bsteng4/SOS+emacs+und+xemacs+mode
(setq clio-dir (getenv "CLIOSOFT_DIR"))
(if clio-dir
   (progn
      (setq clio-emacs-script (concat clio-dir "/scripts/sos_emacs.el"))
      (load-file clio-emacs-script)
      (load-file (concat clio-dir "/scripts/revert_buffs.el"))))
;; /tools/sos/sos_6.32.p2_linux/scripts/sos_emacs.el

;;*=======================
;(require 'dssc)

;;*=======================
;; Set DesignSync/VC options and load it.
(setq vc-sync-work-style 'merging) ; 'merging or 'locking
;;(setq vc-sync-work-style 'locking) ; 'merging or 'locking
(setq vc-initial-comment t)        ; require comment for 'ci -new'
;(add-to-list 'load-path "~/emacs/DesignSync-VC")
;(load "designsync")

;;*=======================
; aka. ghdl-mode
(require 'crb-mode)

;;*=======================
(require 'diff-hl)
(global-diff-hl-mode)

;;*=======================
(require 'printing)
(pr-update-menus)

;;*=======================
;;* turn on Emacs major modes for some filetypes
(setq auto-mode-alist
 (append '(
           (".diary"                   . diary-mode)
           ("\\.sh$"                   . sh-mode)
           ("\\.bashrc"                . sh-mode)
           ("\\.gnus"                  . emacs-lisp-mode)
           ("\\.c$"                    . c++-mode)
           ("\\.h$"                    . c++-mode)
           ("\\.l$"                    . c-mode)
           ("\\.y$"                    . c-mode)
           ("\\.awk$"                  . c-mode)
           ("\\.php3$"                 . c++-mode)
           ("\\.cc$"                   . c++-mode)
           ("\\.x$"                    . c-mode)
           ("\\.s$"                    . asm-mode)
           ("\\.p$"                    . pascal-mode)
           ("\\.txt$"                  . text-mode)
           ("\\.text$"                 . text-mode)
           ("\\.tex$"                  . latex-mode)
           ("\\.sm$"                   . latex-mode)
           ("\\.sty$"                  . latex-mode)
           ("\\.bib$"                  . bibtex-mode)
           ("\\.el$"                   . emacs-lisp-mode)
           ("\\.tcl$"                  . tcl-mode)
           ("\\.sim$"                  . tcl-mode)
           ("\\.java$"                 . java-mode)
           ("\\.html$"                 . html-mode)
           ("\\.sgm$"                  . sgml-mode)
           ("\\.sgml$"                 . sgml-mode)
           ("[]>:/]Makefile"           . makefile-mode)
           ("[]>:/]\\..*emacs"         . emacs-lisp-mode)
           ("\\.rfc$"                  . rfc-mode)
           ("\\.id$"                   . rfc-mode)
           ("\\.isl$"                  . isl-mode)
           ("\\.va$"                   . verilog-mode)
           ("\\.v$"                    . verilog-mode)
           ("\\.vh$"                   . verilog-mode)
           ("\\.vams$"                 . verilog-mode)
           ("\\.dv$"                   . verilog-mode)
           ("\\.va$"                   . verilog-mode)
           ("\\.vsif$"                 . verilog-mode)
           ("\\.vhd$"                  . vhdl-mode)
           ("\\.vhdl$"                 . vhdl-mode)
           ("\\.vhms$"                 . vhdl-mode)
           ("\\.net$"                  . crb-mode)
           )
         auto-mode-alist))

;;*=======================
(ffap-bindings)                      ; do default key bindings

;;*=======================
;;* Filladapt-Mode
(require 'filladapt)
(setq-default filladapt-mode t)
(setq filladapt-fill-column-tolerance 3)
(add-hook 'c-mode-hook 'turn-off-filladapt-mode)

;;*=======================
(require 'redo)
(global-set-key "\C-^" 'redo)         ; for tty-s...
(global-set-key [(control ?6)] 'redo) ; ...and for X
(global-set-key "\M-_" 'redo)        ; universal

;;*=======================
(require 'rb-ifs-mode)
(setq auto-mode-alist
       (append (list
                (cons "\\.cmd\\'" 'rb-ifs-mode)
                (cons "\\.icmd\\'" 'rb-ifs-mode))
               auto-mode-alist))

;;*=======================
(require 'tracefile-mode)
(setq auto-mode-alist
      (append (list
               (cons "trace\\.log" 'tracefile-mode))
               auto-mode-alist))

;;*=======================
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")
;;(load-theme 'zenburn t) ; https://github.com/bbatsov/zenburn-emacs
(load-theme 'solarized-light t) ; https://github.com/bbatsov/solarized-emacs

;;*=======================
(require 'smart-mode-line)
(sml/setup) ; https://github.com/Malabarba/smart-mode-line

(setq x-underline-at-descent-line t) ; https://github.com/bbatsov/solarized-emacs#underline-position-setting-for-x

;;*=======================
(require 'git-gutter-fringe+)

;;*=======================
(setq which-func-unknown "")
(which-function-mode)

;;*=======================
;;* Title of the X window corresponding to the selected frame
;;(setq frame-title-format "This is %S file %f in column %c, line %l Status: %*")
(setq frame-title-format "%f")


;;*=======================
;; see https://www.emacswiki.org/emacs/ForceBackups
(setq vc-make-backup-files t)
(setq version-control t ;; Use version numbers for backups.
      kept-new-versions 10 ;; Number of newest versions to keep.
      kept-old-versions 0 ;; Number of oldest versions to keep.
      delete-old-versions t ;; Don't ask to delete excess backup versions.
      backup-by-copying t) ;; Copy all files, don't rename them.
;; If you want to avoid 'backup-by-copying', you can instead use
;;
;; (setq backup-by-copying-when-linked t)
;;
;; but that makes the second, "per save" backup below not run, since
;; buffers with no backing file on disk are not backed up, and
;; renaming removes the backing file.  The "per session" backup will
;; happen in any case, you'll just have less consistent numbering of
;; per-save backups (i.e. only the second and subsequent save will
;; result in per-save backups).

;; If you want to avoid backing up some files, e.g. large files,
;; then try setting 'backup-enable-predicate'.  You'll want to
;; extend 'normal-backup-enable-predicate', which already avoids
;; things like backing up files in '/tmp'.

;; Default and per-save backups go here:
(setq backup-directory-alist '(("" . "~/.emacs.d/backup/per-save")))

(defun force-backup-of-buffer ()
  ;; Make a special "per session" backup at the first save of each
  ;; emacs session.
  (when (not buffer-backed-up)
    ;; Override the default parameters for per-session backups.
    (let ((backup-directory-alist '(("" . "~/.emacs.d/backup/per-session")))
          (kept-new-versions 3))
      (backup-buffer)))
  ;; Make a "per save" backup on each save.  The first save results in
  ;; both a per-session and a per-save backup, to keep the numbering
  ;; of per-save backups consistent.
  (let ((buffer-backed-up nil))
    (backup-buffer)))

(add-hook 'before-save-hook  'force-backup-of-buffer)

;;*=======================
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)

(add-hook 'prog-mode-hook
          (lambda ()
            (flyspell-prog-mode)
            ))

;;*=======================
;; https://github.com/Malabarba/beacon
(beacon-mode 1)

;;*=======================
(require 'spaceline-config) ; https://github.com/TheBB/spaceline
;(spaceline-spacemacs-theme)
(spaceline-emacs-theme)
;(spaceline-toggle-flycheck-error-on)
;(spaceline-toggle-flycheck-warning-on)
;(spaceline-toggle-flycheck-info-on)

;;*=======================
;;(setq flycheck-mode-line
;;      '(:eval
;;        (pcase flycheck-last-status-change
;;          (`not-checked nil)
;;          (`no-checker (propertize " -" 'face 'warning))
;;          (`running (propertize " ✷" 'face 'success))
;;          (`errored (propertize " !" 'face 'error))
;;          (`finished
;;           (let* ((error-counts (flycheck-count-errors flycheck-current-errors))
;;                  (no-errors (cdr (assq 'error error-counts)))
;;                  (no-warnings (cdr (assq 'warning error-counts)))
;;                  (face (cond (no-errors 'error)
;;                              (no-warnings 'warning)
;;                              (t 'success))))
;;             (propertize (format " %s/%s" (or no-errors 0) (or no-warnings 0))
;;                         'face face)))
;;          (`interrupted " -")
;;          (`suspicious '(propertize " ?" 'face 'warning)))))

(require 'flycheck-pos-tip)
(with-eval-after-load 'flycheck
  (flycheck-pos-tip-mode))

(require 'flycheck-pyflakes)
(add-to-list 'flycheck-disabled-checkers 'python-flake8)
(add-to-list 'flycheck-disabled-checkers 'python-pylint)

;;*=======================
;; http://github.com/nonsequitur/smex/
(require 'smex)
(add-hook 'after-init-hook 'smex-initialize)
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)
;; This is your old M-x.
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

;;*=======================
(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward)

;;*=======================
;;; Original author: ttn@netcom.com, 28-Jan-1996
;;; Modified for multiple lines: Eric (Ludlam)
(defun another-line (num-lines)
  "Copies line, preserving cursor column, and increments any numbers found.
Copies a block of optional NUM-LINES lines.  If no optional argument is given,
then only one line is copied."
  (interactive "p")
  (if (not num-lines) (setq num-lines 0) (setq num-lines (1- num-lines)))
  (let* ((col (current-column))
         (bol (save-excursion (forward-line (- num-lines)) (beginning-of-line) (point)))
         (eol (progn (end-of-line) (point)))
         (line (buffer-substring bol eol)))
    (goto-char bol)
    (while (re-search-forward "[0-9]+" eol 1)
      (let ((num (string-to-int (buffer-substring
                                  (match-beginning 0) (match-end 0)))))
        (replace-match (int-to-string (1+ num))))
      (setq eol (save-excursion (goto-char eol) (end-of-line) (point))))
    (goto-char bol)
    (insert line "\n")
    (move-to-column col)))
(define-key global-map (kbd "M-o") 'another-line)

;;*=======================
;;* highlight FIXME etc.
;(setq fixme-keywords '(("\\<\\(FIXME.*\\)" 1 font-lock-warning-face prepend)))
;(setq fixme-keywords '(("\\<\\(FIXME.*\\|foo.*\\)" 1 font-lock-warning-face prepend)))
(setq fixme-keywords '(("\\<\\(FIXME.*\\|%%.*\\)" 1 font-lock-warning-face prepend)))
;(setq fixme-keywords '(("\\<\\(FIXME.*\\|%%.*\\)" 1 font-lock-warning-face set)))
(font-lock-add-keywords 'emacs-lisp-mode fixme-keywords)
(font-lock-add-keywords 'c-mode fixme-keywords)
(font-lock-add-keywords 'c++-mode fixme-keywords)
(font-lock-add-keywords 'LaTeX-mode fixme-keywords)
(font-lock-add-keywords 'latex-mode fixme-keywords)
(font-lock-add-keywords 'makefile-mode fixme-keywords)
(font-lock-add-keywords 'tcl-mode fixme-keywords)
(font-lock-add-keywords 'sh-mode fixme-keywords)
(font-lock-add-keywords 'verilog-mode fixme-keywords)
(font-lock-add-keywords 'vhdl-mode fixme-keywords)
(font-lock-add-keywords 'asm-mode fixme-keywords)
(font-lock-add-keywords 'ncsimlog-mode fixme-keywords)
(font-lock-add-keywords 'octave-mode fixme-keywords)
(font-lock-add-keywords 'nxml-mode fixme-keywords)
(font-lock-add-keywords 'python-mode fixme-keywords)
(font-lock-add-keywords 'rb-ifs-mode fixme-keywords)
(font-lock-add-keywords 'specman-mode fixme-keywords)
(font-lock-add-keywords 'text-mode fixme-keywords)
;; foo fgff
;; %% lklkl
(make-face 'colin-todo-face)
(set-face-attribute 'colin-todo-face nil :foreground "Dark Orange" :weight 'bold)
(setq todo-keywords '(("\\<\\(TODO.*\\)" 1 'colin-todo-face prepend)))
(font-lock-add-keywords 'emacs-lisp-mode todo-keywords)
(font-lock-add-keywords 'c-mode todo-keywords)
(font-lock-add-keywords 'c++-mode todo-keywords)
(font-lock-add-keywords 'LaTeX-mode todo-keywords)
(font-lock-add-keywords 'latex-mode todo-keywords)
(font-lock-add-keywords 'makefile-mode todo-keywords)
(font-lock-add-keywords 'tcl-mode todo-keywords)
(font-lock-add-keywords 'sh-mode todo-keywords)
(font-lock-add-keywords 'verilog-mode todo-keywords)
(font-lock-add-keywords 'vhdl-mode todo-keywords)
(font-lock-add-keywords 'asm-mode todo-keywords)
(font-lock-add-keywords 'ncsimlog-mode todo-keywords)
(font-lock-add-keywords 'octave-mode todo-keywords)
(font-lock-add-keywords 'nxml-mode todo-keywords)
(font-lock-add-keywords 'python-mode todo-keywords)
(font-lock-add-keywords 'rb-ifs-mode todo-keywords)
(font-lock-add-keywords 'specman-mode todo-keywords)
(font-lock-add-keywords 'text-mode todo-keywords)
;; TODO: foo!
(make-face 'colin-note-face)
(set-face-attribute 'colin-note-face nil :foreground "Medium Sea Green" :weight 'bold)
(setq note-keywords '(("\\<\\(NOTE.*\\)" 1 'colin-note-face prepend)))
(font-lock-add-keywords 'emacs-lisp-mode note-keywords)
(font-lock-add-keywords 'c-mode note-keywords)
(font-lock-add-keywords 'c++-mode note-keywords)
(font-lock-add-keywords 'LaTeX-mode note-keywords)
(font-lock-add-keywords 'latex-mode note-keywords)
(font-lock-add-keywords 'makefile-mode note-keywords)
(font-lock-add-keywords 'tcl-mode note-keywords)
(font-lock-add-keywords 'sh-mode note-keywords)
(font-lock-add-keywords 'verilog-mode note-keywords)
(font-lock-add-keywords 'vhdl-mode note-keywords)
(font-lock-add-keywords 'asm-mode note-keywords)
(font-lock-add-keywords 'ncsimlog-mode note-keywords)
(font-lock-add-keywords 'octave-mode note-keywords)
(font-lock-add-keywords 'nxml-mode note-keywords)
(font-lock-add-keywords 'python-mode note-keywords)
(font-lock-add-keywords 'rb-ifs-mode note-keywords)
(font-lock-add-keywords 'specman-mode note-keywords)
(font-lock-add-keywords 'text-mode note-keywords)
;; NOTE: foo!

;;* from Reto Zimmermann (completion):
(defun indent-for-tab-command (&optional prefix-arg)
   "If preceding character is part of a word then dabbrev-expand,
else if right of non-whitespace on line then tab-to-tab-stop,
else indent line in proper way for current major mode
\(overrides and extends original indent-for-tab-command function)."
   (interactive "P")
   (cond ((= (char-syntax (preceding-char)) ?w)
;;          (let ((case-fold-search t)) (dabbrev-expand prefix-arg)))
          (let ((case-fold-search nil)) (dabbrev-expand prefix-arg)))
         ((> (current-column) (current-indentation))
          (tab-to-tab-stop))
         (t
          (if (eq indent-line-function 'indent-to-left-margin)
              (insert-tab prefix-arg)
            (if prefix-arg
                (funcall indent-line-function prefix-arg)
              (funcall indent-line-function))))))
(add-hook 'verilog-mode-hook
	  '(lambda ()
	     (local-set-key '[tab] 'indent-for-tab-command)
	     ))
(add-hook 'octave-mode-hook
	  '(lambda ()
	     (local-set-key '[tab] 'indent-for-tab-command)
	     ))
(add-hook 'specman-mode-hook
	  '(lambda ()
	     (local-set-key '[tab] 'indent-for-tab-command)
	     ))

;;*=======================
;; If necessary, automatically update the year for the copyright notice:
;;(add-hook 'write-file-functions 'copyright-fix-years)
(add-hook 'write-file-functions 'copyright-update)
;;(setq copyright-query nil)

;;*=======================
;;(global-set-key [S-mouse-2] 'browse-url-at-mouse)
;; better: goto-address-(prog-)mode
;; for links, URLs, http
(add-hook 'vhdl-mode-hook
          (lambda ()
            (goto-address-prog-mode)
            ))
(add-hook 'verilog-mode-hook
          (lambda ()
            (goto-address-prog-mode)
            ))
(add-hook 'python-mode-hook
          (lambda ()
            (goto-address-prog-mode)
            ))
(add-hook 'rb-ifs-mode-hook
          (lambda ()
            (goto-address-prog-mode)
            ))

;;*=======================
; http://www.emacswiki.org/emacs/WindMove
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))

;;*=======================
;; adapted from https://stackoverflow.com/questions/9288181/converting-from-camelcase-to-in-emacs
(defun toggle-camelcase-underscores ()
  "Toggle between camelcase and underscore notation for the symbol at point."
  (interactive)
  (save-excursion
    (let* ((bounds (bounds-of-thing-at-point 'symbol))
           (start (car bounds))
           (end (cdr bounds))
           (currently-using-underscores-p (progn (goto-char start)
                                                 (re-search-forward "_" end t))))
      (if currently-using-underscores-p
          (progn
            (upcase-initials-region start end)
            (replace-string "_" "" nil start end)
            (downcase-region start (1+ start))
            )
        (replace-regexp "\\([A-Z]\\)" "_\\1" nil (1+ start) end)
        (upcase-region start end)))))

;;*=======================
;; https://stackoverflow.com/questions/9288181/converting-from-camelcase-to-in-emacs
(defun x-mark-missing-files ()
  (interactive)
  (save-excursion
   (while (search-forward-regexp "~?/[A-Za-z0-9_./-]+")
     (when (not (file-exists-p (match-string 0)))
       (overlay-put
        (make-overlay (match-beginning 0) (match-end 0))
        'face '(:background "red"))))))

;;*=======================
;;; from http://blog.tuxicity.se/elisp/emacs/2010/03/26/rename-file-and-buffer-in-emacs.html
(defun rename-file-and-buffer ()
  "Renames current buffer and file it is visiting."
  (interactive)
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not (and filename (file-exists-p filename)))
        (message "Buffer '%s' is not visiting a file!" name)
      (let ((new-name (read-file-name "New name: " filename)))
        (cond ((get-buffer new-name)
               (message "A buffer named '%s' already exists!" new-name))
              (t
               (rename-file filename new-name 1)
               (rename-buffer new-name)
               (set-visited-file-name new-name)
               (set-buffer-modified-p nil)))))))
(global-set-key (kbd "C-c r") 'rename-file-and-buffer)

;;*=======================
;; from http://emacswiki.org/emacs/RevertBuffer
(defun revert-all-buffers ()
  "Refreshes all open buffers from their respective files."
  (interactive)
  (dolist (buf (buffer-list))
    (with-current-buffer buf
      (when (and (buffer-file-name) (not (buffer-modified-p)))
        (revert-buffer t t t) )))
  (message "Refreshed open files.") )

;;*=======================
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(browse-url-generic-program "firefox")
 '(calculator-2s-complement t)
 '(calculator-bind-escape nil)
 '(calculator-number-digits 7)
 '(column-number-mode t)
 '(custom-enabled-themes (quote (solarized-light)))
 '(custom-safe-themes
   (quote
    ("a27c00821ccfd5a78b01e4f35dc056706dd9ede09a8b90c6955ae6a390eb1c1e" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" "c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "d677ef584c6dfc0697901a44b885cc18e206f05114c8a3b7fde674fce6180879" "a8245b7cc985a0610d71f9852e9f2767ad1b852c2bdea6f4aadc12cce9c4d6d0" default)))
 '(display-time-mode t)
 '(ediff-custom-diff-options "-u")
 '(ediff-split-window-function (quote split-window-horizontally))
 '(ediff-window-setup-function (quote ediff-setup-windows-plain))
 '(eudc-inline-expansion-format (quote ("%s <%s>" displayname email)))
 '(eudc-protocol (quote ldap))
 '(eudc-strict-return-matches nil)
 '(fill-column 120)
 '(flycheck-checkers
   (quote
    (ada-gnat asciidoc c/c++-clang c/c++-gcc c/c++-cppcheck cfengine chef-foodcritic coffee coffee-coffeelint coq css-csslint d-dmd emacs-lisp emacs-lisp-checkdoc erlang eruby-erubis fortran-gfortran go-gofmt go-golint go-vet go-build go-test go-errcheck go-unconvert groovy haml handlebars haskell-stack-ghc haskell-ghc haskell-hlint html-tidy jade javascript-eslint javascript-jshint javascript-gjslint javascript-jscs javascript-standard json-jsonlint json-python-json less lua-luacheck lua perl perl-perlcritic php php-phpmd php-phpcs processing puppet-parser puppet-lint python-pyflakes python-pycompile r-lintr racket rpm-rpmlint markdown-mdl rst-sphinx rst ruby-rubocop ruby-rubylint ruby ruby-jruby rust-cargo rust sass scala scala-scalastyle scss-lint scss sh-bash sh-posix-dash sh-posix-bash sh-zsh sh-shellcheck slim sql-sqlint tex-chktex tex-lacheck texinfo typescript-tslint verilog-verilator xml-xmlstarlet xml-xmllint yaml-jsyaml yaml-ruby)))
 '(flycheck-python-flake8-executable nil)
 '(fringe-indicators (quote empty) nil (fringe))
 '(fringe-mode (quote (5 . 5)) nil (fringe))
 '(git-gutter-fr:side (quote left-fringe))
 '(global-auto-highlight-symbol-mode t)
 '(global-linum-mode t)
 '(global-whitespace-mode t)
 '(hideshowvis-ignore-same-line nil)
 '(indent-tabs-mode nil)
 '(indicate-buffer-boundaries (quote right))
 '(indicate-empty-lines t)
 '(inhibit-startup-screen t)
 '(ldap-host-parameters-alist
   (quote
    (("ldap://de.bosch.com:3268" base "dc=de,dc=bosch,dc=com" binddn "de\\maq3rt" passwd ldap-password-read auth simple))))
 '(mouse-avoidance-mode (quote animate) nil (avoid))
 '(package-selected-packages
   (quote
    (try persistent-soft confluence mediawiki minimap sublimity popup ## strace-mode diminish use-package hideshowvis bbdb wgrep websocket web-mode sr-speedbar spaceline solarized-theme smex smart-mode-line session redo+ rainbow-mode rainbow-delimiters nlinum multiple-cursors markdown-mode magit list-unicode-display highlight-symbol highlight git-gutter-fringe git-gutter-fringe+ font-lock-studio flycheck-pyflakes flycheck-pos-tip fish-mode diff-hl describe-number csv-mode counsel beacon ack)))
 '(password-cache-expiry nil)
 '(safe-local-variable-values
   (quote
    ((engine . mako)
     (encoding . ascii)
     (eval truncate-lines 1))))
 '(scroll-bar-mode (quote right))
 '(scroll-error-top-bottom t)
 '(scroll-preserve-screen-position t)
 '(send-mail-function (quote smtpmail-send-it))
 '(session-use-package t nil (session))
 '(shell-file-name "/bin/bash")
 '(show-paren-mode t)
 '(size-indication-mode t)
 '(sml/pre-modes-separator (propertize " " (quote face) (quote sml/modes)))
 '(sml/theme (quote respectful))
 '(smtpmail-smtp-server "rb-smtp-int.bosch.com")
 '(smtpmail-smtp-service 25)
 '(solarized-distinct-fringe-background t)
 '(solarized-high-contrast-mode-line t)
 '(solarized-use-less-bold t)
 '(specman-company "Bosch Sensortec GmbH")
 '(specman-compile-command
   "/tools/cds/incisive_15.20.013_Linux/specman/bin/sn_compile.sh ")
 '(specman-continued-line-offset 4)
 '(specman-max-line-length 120)
 '(speedbar-use-imenu-flag t)
 '(sr-speedbar-right-side nil)
 '(sublimity-attractive-centering-width nil)
 '(sublimity-map-size 15)
 '(sublimity-map-text-scale -8)
 '(sublimity-mode t)
 '(tool-bar-mode nil)
 '(verilog-auto-newline nil)
 '(verilog-highlight-translate-off t)
 '(verilog-indent-level 2)
 '(verilog-indent-level-behavioral 2)
 '(verilog-indent-level-declaration 2)
 '(verilog-indent-level-directive 2)
 '(verilog-indent-level-module 2)
 '(verilog-indent-lists t)
 '(vhdl-architecture-file-name (quote ("\\(.*\\) \\(.*\\)" . "\\1.\\2")))
 '(vhdl-basic-offset 2)
 '(vhdl-entity-file-name (quote (".*" . "\\&.entity")))
 '(vhdl-forbidden-syntax
   "\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w+")
 '(vhdl-highlight-case-sensitive nil)
 '(vhdl-highlight-forbidden-words t)
 '(vhdl-highlight-special-words t)
 '(vhdl-highlight-translate-off t)
 '(vhdl-highlight-verilog-keywords t)
 '(vhdl-include-direction-comments t)
 '(vhdl-include-group-comments (quote always))
 '(vhdl-include-type-comments t)
 '(vhdl-project-alist
   (quote
    (("TB285BA" "" "/projects/genpp6/tb285ba/maq3rt/current/database_tb285_dig/tb285_dig/"
      ("rtl_vhd/*.vhd" "rtl_vhd/generated/*.vhd" "tb_vhd/*.vhd" "tb_vhd/generated/*.vhd")
      ""
      (("ModelSim" "-87 \\2" "-f \\1 top_level" nil)
       ("Synopsys" "-vhdl87 \\2" "-f \\1 top_level"
        ((".*/datapath/.*" . "-optimize \\3")
         (".*_tb\\.vhd"))))
      "lib/" "example3_lib" "lib/example3/" "Makefile_\\2" "")
     ("TB185CA" "" "/projects/genpp6/tb185ca/maq3rt/current/database_tb185_dig/tb185_dig/"
      ("rtl_vhd/*.vhd" "rtl_vhd/generated/*.vhd" "tb_vhd/*.vhd" "tb_vhd/generated/*.vhd")
      ""
      (("ModelSim" "-87 \\2" "-f \\1 top_level" nil)
       ("Synopsys" "-vhdl87 \\2" "-f \\1 top_level"
        ((".*/datapath/.*" . "-optimize \\3")
         (".*_tb\\.vhd"))))
      "lib/" "example3_lib" "lib/example3/" "Makefile_\\2" "")
     ("RBTULIB 2.3" "" "/tools/rbtulib/v2.3/"
      ("*/*.vhd" "*/*/*.vhd" "*/*/*/*.vhd" "*/*/*/*.vhms" "*/*/*.vhms" "*/*.vhms" "-r ./")
      ""
      (("ModelSim" "-87 \\2" "-f \\1 top_level" nil)
       ("Synopsys" "-vhdl87 \\2" "-f \\1 top_level"
        ((".*/datapath/.*" . "-optimize \\3")
         (".*_tb\\.vhd"))))
      "lib/" "example3_lib" "lib/example3/" "Makefile_\\2" ""))))
 '(vhdl-speedbar-display-mode (quote project))
 '(vhdl-speedbar-jump-to-unit nil)
 '(vhdl-standard (quote (93 (ams math))))
 '(vhdl-testbench-architecture-name (quote (".*" . "SimulationModel")))
 '(vhdl-testbench-configuration-name (quote ("\\(.*\\) \\(.*\\)" . "\\1\\2")))
 '(vhdl-testbench-create-files (quote separate))
 '(vhdl-testbench-declarations "  -- main clock
  signal clk_main : std_ulogic := '1';
")
 '(vhdl-testbench-entity-name (quote (".*" . "Tb\\&")))
 '(vhdl-testbench-statements
   "  -- clock generation
  clk_Main <= not clk_Main after c_ClkMainPeriod;

  -- waveform generation
  p_WaveGen: process
  begin
    -- insert signal assignments here

    wait until clk_Main = '1';
  end process p_WaveGen;
")
 '(web-mode-enable-auto-indentation nil)
 '(web-mode-enable-engine-detection t)
 '(whitespace-line-column 120)
 '(whitespace-style (quote (face tab-mark tab-mark))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(git-gutter-fr:added ((t (:foreground "#859900" :weight normal))))
 '(git-gutter-fr:deleted ((t (:foreground "#dc322f" :weight normal))))
 '(git-gutter-fr:modified ((t (:foreground "#268bd2" :weight normal))))
 '(git-gutter:separator ((t (:inherit default :foreground "cyan" :weight normal))))
 '(link ((t (:foreground "#b58900" :underline t :weight normal)))))


;;(setq package-list '(persistent-soft confluence mediawiki minimap sublimity popup strace-mode diminish use-package hideshowvis bbdb wgrep websocket web-mode sr-speedbar spaceline solarized-theme smex smart-mode-line session redo+ rainbow-mode rainbow-delimiters nlinum multiple-cursors markdown-mode magit list-unicode-display highlight-symbol highlight git-gutter-fringe git-gutter-fringe+ font-lock-studio flycheck-pyflakes flycheck-pos-tip fish-mode diff-hl describe-number csv-mode counsel beacon ack))
;;; activate all the packages (in particular autoloads)
;;(package-initialize)
;;
;;; fetch the list of packages available 
;;(unless package-archive-contents
;;  (package-refresh-contents))
;;
;;; install the missing packages
;;(dolist (package package-list)
;;  (unless (package-installed-p package)
;;    (package-install package)))
;;
